/**
* @Name           : Shift_AccountHandler
* @Author         : Shift CRM
* @Description    : Account trigger for data loading
* @Sharing        : Without sharing to run as system
*
* Maintenance History:
* Date ------------ 	Name  ---- 		Version --- 	Remarks --------------------
* 02/26/2016 			Desmond Li		1.0 			Initial
*/

public with sharing class Shift_AccountHandler {

	private static String PARENTING_ERRROR = 'Unable to parent this Account record';
	private static String RETAILER_TYPE = 'Retailer';

	/**
	 * Parent the data load records with the appropriate Parent Account and Referred By Account
	 * @param List<Account> list of accounts that are being loaded
	 */
	public static void parentAccountProcess(List<Account> accountList) {

		// Compile Set Id to query out the lookup accounts we need to populate Account fields
		Set<String> distIdSet = new Set<String>();
		for (Integer i = 0, j = accountList.size(); i < j ; i++) {
			if (accountList[i].Type == RETAILER_TYPE) {
				if (accountList[i].Distribution_Id__c <> null) {
					distIdSet.add(accountList[i].Distribution_Id__c);	
				}
			}
		}

		if (!distIdSet.isEmpty()) {
			List<Account> lookupAccounts = [SELECT Id, Customer_ID__c FROM Account WHERE Customer_ID__c IN :distIdSet];
			if (!lookupAccounts.isEmpty()) {
				try {
					Map<String, Id> distIdMap = new Map<String, Id>();
					for (Integer i = 0, j = lookupAccounts.size(); i < j ; i++) {
						//distIdMap.put(lookupAccounts[i].Distribution_Id__c, lookupAccounts[i].Id);
						distIdMap.put(lookupAccounts[i].Customer_ID__c, lookupAccounts[i].Id);
					}
					System.debug('lookupAccounts: ' + lookupAccounts);
					for (Integer i = 0, j = accountList.size(); i < j ; i++) {
						setAccountLookupFields(accountList[i], distIdMap.get(accountList[i].Distribution_Id__c));
					}
				} catch (Exception e) {
					System.debug('Exception: ' + e);
				}
			}
		}
	}

	/**
	 * Set the parent lookup fields for the given Account record
	 * @param Account record to parent
	 * @param Id - the Id to parent with
	 */
	private static void setAccountLookupFields(Account tempAccount, Id accountId) {
		try {
			tempAccount.ParentId = accountId;
			tempAccount.Referred_By_Account__c = accountId;
		} catch (Exception e) {
			tempAccount.addError(PARENTING_ERRROR + ': ' + e);
		}
	}
}