/**
* @Name           : Shift_AccountHandler_Test
* @Author         : Shift CRM
* @Description    : Test class for Shift_AccountHandler
*
* Maintenance History:
* Date ------------ 	Name  ---- 		Version --- 	Remarks --------------------
* 02/26/2016 			Desmond Li		1.0 			Initial
*/

@isTest
private class Shift_SalesDataHandler_Test {

	/**
	* Single record Test
	*/
	static testMethod void singleTest() {
		Integer maximumSize = 1;
		List<Account> parentLst = createParentAccount(maximumSize);
		List<Sales_Data__c> childLst = createSalesData(maximumSize, -100);
		insert childLst;
		List<Sales_Data__c> assertLst = [SELECT Id, Retailer_s_account_number__c, Distributor_Name__c FROM Sales_Data__c WHERE Retailer_s_account_number__c <> null];
		System.debug('assertLst: ' + assertLst);
		System.assertEquals(maximumSize, assertLst.size());
	}

	/**
	* Bulk record Test
	*/
	static testMethod void bulkTest() {
		Integer maximumSize = 500;
		List<Account> parentLst = createParentAccount(maximumSize);
		List<Sales_Data__c> childLst = createSalesData(maximumSize, 100);
		insert childLst;
		List<Sales_Data__c> assertLst = [SELECT Id, Retailer_s_account_number__c, Distributor_Name__c FROM Sales_Data__c WHERE Retailer_s_account_number__c <> null];
		System.debug('assertLst: ' + assertLst);
		System.assertEquals(maximumSize, assertLst.size());
	}

	/**
	* Create Parent Account test records
	* @param Integer maximumSize
	* @return List of accounts
	*/
	private static List<Account> createParentAccount(Integer maximumSize) {
		List<Account> accountLst = new List<Account>();

		for (Integer i = 1 ; i <= maximumSize ; i++) {
			accountLst.add(new Account(
			                   Name = 'TestAccount' + i,
			                   Type = 'Distributor / Wholesaler',
			                   Customer_ID__c = String.valueOf(i)));
		}

		for (Integer i = 1 ; i <= maximumSize ; i++) {
			accountLst.add(new Account(
			                   Name = 'TestAccount:' + 'D' + i + '-R' + i,
			                   Type = 'Distributor / Wholesaler',
			                   Customer_ID__c = 'D' + i + '-R' + i ));
		}

		insert accountLst;
		return accountLst;
	}

	/**
	* Create Sales Data test records
	* @param Integer maximumSize
	* @return List of Sales Data records
	*/
	private static List<Sales_Data__c> createSalesData(Integer maximumSize, Integer quantity) {
		List<Sales_Data__c> salesDataLst = new List<Sales_Data__c>();
		for (Integer i = 1 ; i <= maximumSize ; i++) {
			salesDataLst.add(new Sales_Data__c(
			                     Name = 'TestSalesData' + i,
			                     Distribution_Id__c = String.valueOf(i),
			                     Invoice_And_Product_And_Date__c = System.today().year() + '-' + i,
			                     Quantity__c = quantity,
			                     Account_Number__c = String.valueOf(i)));
		}
		return salesDataLst;
	}

	/**
	* Custom Settings
	*/
	@testSetup
	private static void setup() {
		Data_Load_Settings__c customSettings = new Data_Load_Settings__c(
		    Enable_Account_Parenting__c   = true,
		    Enable_SalesData_Parenting__c = true
		);
		insert customSettings;
	}
}