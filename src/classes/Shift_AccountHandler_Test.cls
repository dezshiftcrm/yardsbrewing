/**
* @Name           : Shift_AccountHandler_Test
* @Author         : Shift CRM
* @Description    : Test class for Shift_AccountHandler
*
* Maintenance History:
* Date ------------ 	Name  ---- 		Version --- 	Remarks --------------------
* 02/26/2016 			Desmond Li		1.0 			Initial
*/

@isTest
private class Shift_AccountHandler_Test {

	/**
	* Single record Test
	*/
	static testMethod void singleTest() {
		Integer maximumAccount = 1;
		List<Account> parentLst = createParentAccount(maximumAccount);
		List<Account> childLst = createChildAccount(maximumAccount);
		insert childLst;
		List<Account> assertLst = [SELECT Id FROM Account WHERE ParentId <> null];
		System.assertEquals(maximumAccount, assertLst.size());
	}

	/**
	* Bulk record Test
	*/
	static testMethod void bulkTest() {
		Integer maximumAccount = 500;
		List<Account> parentLst = createParentAccount(maximumAccount);
		List<Account> childLst = createChildAccount(maximumAccount);
		insert childLst;
		List<Account> assertLst = [SELECT Id FROM Account WHERE ParentId <> null];
		System.assertEquals(maximumAccount, assertLst.size());
	}

	/**
	* Create Parent Account test records
	* @param Integer maximumAccount
	* @return List of accounts
	*/
	private static List<Account> createParentAccount(Integer maximumAccount) {
		List<Account> accountLst = new List<Account>();

		for (Integer i = 1 ; i <= maximumAccount ; i++) {
			accountLst.add(new Account(
									Name = 'TestAccount' + i,
									Type = 'Distributor / Wholesaler',
									Customer_ID__c = String.valueOf(i)));
		}
		insert accountLst;
		return accountLst;
	}

	/**
	* Create child Account test records
	* @param Integer maximumAccount
	* @return List of accounts
	*/
	private static List<Account> createChildAccount(Integer maximumAccount) {
		List<Account> accountLst = new List<Account>();
		for (Integer i = 1 ; i <= maximumAccount ; i++) {
			accountLst.add(new Account(
									Name = 'TestAccount' + i,
									Type = 'Retailer',
									Distribution_Id__c = String.valueOf(i)));
		}

		return accountLst;
	}

	/**
	* Custom Settings
	* @return void
	*/
	@testSetup
	private static void setup() {
		Data_Load_Settings__c customSettings = new Data_Load_Settings__c(
			Enable_Account_Parenting__c   = true,
			Enable_SalesData_Parenting__c = true
			);
		insert customSettings;
	}
}