/**
* @Name           : Shift_SalesDataHandler
* @Author         : Shift CRM
* @Description    : Sales Data trigger for data loading
* @Sharing        : Without sharing to run as system
*
* Maintenance History:
* Date ------------ 	Name  ---- 		Version --- 	Remarks --------------------
* 02/26/2016 			Desmond Li		1.0 			Initial
*/

public with sharing class Shift_SalesDataHandler {

	private static String PARENTING_ERRROR = 'Unable to parent this Sales Data record';
	private static String RETAILID_ERRROR  = 'Unable to generate Retailer Id';

	/**
	* Used by trigger to parent inserted Sales Data records
	* @param List<Sales_Data__c> salesDataList
	* @return void
	*/
	public static void parentSalesDataProcess(List<Sales_Data__c> salesDataList) {

		// Compile Set Id to query out the lookup accounts we need to populate Account fields
		Set<String> retailIdSet = new Set<String>();
		for (Integer i = 0, j = salesDataList.size(); i < j ; i++) {
			String retailId = makeRetailerId(salesDataList[i]);
			retailIdSet.add(retailId);
			salesDataList[i].Retailer_Customer_Id__c = retailId;
		}
		// Create map to associate with Retailer Account Number
		List<Account> lookupRetail = [SELECT Id, Customer_ID__c FROM Account WHERE Customer_ID__c IN :retailIdSet];
		Map<String, String> retailLookupMap = new Map<String, String>();
		if (!lookupRetail.isEmpty())  {
			for (Account a : lookupRetail) {
				retailLookupMap.put(a.Customer_ID__c, a.Id);
			}
		}

		// Compile Set Id to query out the lookup accounts we need to populate Account fields
		Set<String> distIdSet = new Set<String>();
		for (Integer i = 0, j = salesDataList.size(); i < j ; i++) {
			distIdSet.add(salesDataList[i].Distribution_Id__c);
		}
		// Generate map to parent Distributor field
		List<Account> lookupAccounts = [SELECT Id, Customer_ID__c FROM Account WHERE Customer_ID__c IN :distIdSet];
		Map<String, Id> distIdMap = new Map<String, Id>();
		if (!lookupAccounts.isEmpty()) {
			for (Integer i = 0, j = lookupAccounts.size(); i < j ; i++) {
				distIdMap.put(lookupAccounts[i].Customer_ID__c, lookupAccounts[i].Id);
			}
		}
		// Set Parent fields and discount fields
		for (Integer i = 0, j = salesDataList.size(); i < j ; i++) {
			setSalesDataParentFields(salesDataList[i], retailLookupMap.get(salesDataList[i].Retailer_Customer_Id__c), distIdMap.get(salesDataList[i].Distribution_Id__c) );
			setDiscountFields(salesDataList[i]);
		}
	}

	/**
	* Set the discount fields based off of the Quantity
	* @param Sales_Data__c salesObj
	* @return void
	*/
	private static void setDiscountFields(Sales_Data__c salesObj) {

		// If either Front or NetPrice are null do nothing
		if (salesObj.Front__c <> null) {
			if (salesObj.Quantity__c >= 0) {
				salesObj.Price_per_unit_no_discount__c = salesObj.Front__c;
			} else {
				salesObj.Price_per_unit_no_discount__c = -salesObj.Front__c;
			}
		}

		if (salesObj.NetPrice__c <> null) {
			if (salesObj.Quantity__c >= 0) {
				salesObj.Price_per_unit_with_discount__c = salesObj.NetPrice__c;
			} else {
				salesObj.Price_per_unit_with_discount__c = -salesObj.NetPrice__c;
			}
		}

	}

	/**
	* Associate with appropriate Account Ids
	* @param Sales_Data__c salesObj
	* @param Id retailId 
	* @param Id distId
	* @return void
	*/
	private static void setSalesDataParentFields(Sales_Data__c salesObj, Id retailId, Id distId) {
		try {
			salesObj.Retailer_s_account_number__c = retailId;
			salesObj.Distributor_Name__c = distId;
		} catch (Exception e) {
			salesObj.addError(PARENTING_ERRROR);
		}
	}

	/**
	* Generate Retail Id based off of Distribution Id or Account Number
	*/
	private static String makeRetailerId(Sales_Data__c salesObj) {
		String retailId = '';
		try {
			
			retailId = 'D' + salesObj.Distribution_Id__c + '-R' + salesObj.Account_Number__c;
		} catch (Exception e) {
			salesObj.addError(RETAILID_ERRROR);
		}
		return retailId;
	}
}