/**
* @Name           : Shift_SalesData
* @Author         : Shift CRM
* @Description    : Sales Data trigger for data loading
*
* Maintenance History:
* Date ------------ 	Name  ---- 		Version --- 	Remarks --------------------
* 02/26/2016 			Desmond Li		1.0 			Initial
*/

trigger Shift_SalesData on Sales_data__c (before insert) {
	Boolean enableParenting = Data_Load_Settings__c.getOrgDefaults().Enable_SalesData_Parenting__c;
	if (Trigger.isBefore && Trigger.isInsert) {
		if (enableParenting) {
			Shift_SalesDataHandler.parentSalesDataProcess(Trigger.new);
		}
	}
}