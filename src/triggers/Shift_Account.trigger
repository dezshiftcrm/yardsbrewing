/**
* @Name           : Shift_Account
* @Author         : Shift CRM
* @Description    : Account trigger for data loading
*
* Maintenance History:
* Date ------------ 	Name  ---- 		Version --- 	Remarks --------------------
* 02/26/2016 			Desmond Li		1.0 			Initial
*/

trigger Shift_Account on Account (before insert) {
	Boolean enableParenting = Data_Load_Settings__c.getOrgDefaults().Enable_Account_Parenting__c;
	if (Trigger.isBefore && Trigger.isInsert) {
		if (enableParenting) {
			Shift_AccountHandler.parentAccountProcess(Trigger.new);
		}
	}
}